package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.inputHandling.CommandExecutorImpl;

import java.util.Arrays;
import java.util.List;

public class QueryCommand implements Command {

    /**
     * Checks all the requirements before using the execute method. Throws errors with messages to handle it
     * in the CommandExecutorImpl
     * @param command
     * @throws CommandException message explains what was the problem in the command
     * @throws NumberFormatException
     */
    public void validateCommand(List<String> command, CommandExecutorImpl.ProgramState programState) throws CommandException  {
        CommandType prevCommand = programState.getPrevCommand();
        StringBuilder str = new StringBuilder();
        RaceDB db = RaceDB.getInstance();
        if(!Arrays.asList(CommandType.FINISH, CommandType.QUERY, CommandType.POINT).contains(prevCommand)){
            str.append("Required previous command was not met. Previous command: ");
            str.append(prevCommand);
            throw new CommandException(str.toString());
        }
        if(prevCommand == CommandType.QUERY){
            System.err.println("Previous command was also a QUERY, which was override now");
        }
        if(command.size() < 2 ||  3 < command.size() ){
            throw new CommandException("Wrong number of command arguments. It should be 2 or 3 with commandType");
        }
        try{
            Integer queryEndRaceNum = -1;
            Integer queryYear = Integer.valueOf(command.get(1));
            if(command.size() == 3){
                queryEndRaceNum = Integer.valueOf(command.get(2));
            }
            if(!db.containsYear(queryYear)){
                throw new CommandException("There is no data about any race in the given year");
            }
            if(db.getYear(queryYear).highestEndRaceNum() < queryEndRaceNum){
                throw new CommandException("There are less Races in the given year than the specified EndRaceNum value");
            }
        } catch (NumberFormatException e){
            throw new CommandException("Either the Year or EndRaceNumber value couldn't be converted to a number");
        }
    }
    /**
     * Sets Query information
     */
    public void execute(List<String> command, CommandExecutorImpl.ProgramState programState) {
        CommandType commandType = CommandType.valueOf(command.get(0));

        Integer queryYear = Integer.valueOf(command.get(1));
        int queryEndRaceNum;
        if(command.size() == 3){
            queryEndRaceNum = Integer.parseInt(command.get(2));
        } else {
            queryEndRaceNum = -1;
        }

        // Execute QUERY command
        programState.setQueryYear(queryYear);
        programState.setQueryEndRaceNum(queryEndRaceNum);
        programState.setPrevCommand(commandType);
    }
}
