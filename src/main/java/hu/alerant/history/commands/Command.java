package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.inputHandling.CommandExecutorImpl;

import java.util.List;

public interface Command {

    /**
     * Validates the command before execution and throws CommandException-t with message about the errors details
     * @param command
     * @throws CommandException
     */
    void validateCommand(List<String> command, CommandExecutorImpl.ProgramState programState) throws CommandException;

    /**
     * Executes the command knowing it is already validated
     * @param command
     */
    void execute(List<String> command, CommandExecutorImpl.ProgramState programState);
}
