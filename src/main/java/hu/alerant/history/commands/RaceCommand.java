package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.Race;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.data.RaceSeason;
import hu.alerant.history.inputHandling.CommandExecutorImpl;

import java.util.Arrays;
import java.util.List;

public class RaceCommand implements Command {

    /**
     * Checks all the requirements before using the execute method. Throws errors with messages to handle it
     * in the CommandExecutorImpl
     * @param command
     * @throws CommandException message explains what was the problem in the command
     * @throws NumberFormatException
     */
    public void validateCommand(List<String> command, CommandExecutorImpl.ProgramState programState) throws CommandException  {
        CommandType prevCommand = programState.getPrevCommand();
        StringBuilder str = new StringBuilder();
        RaceDB db = RaceDB.getInstance();
        if(!Arrays.asList(CommandType.FINISH, CommandType.POINT).contains(prevCommand)){
            str.append("Required previous command was not met. Previous command: ");
            str.append(prevCommand);
            throw new CommandException(str.toString());
        }
        if(command.size() != 5){
            throw new CommandException("Wrong number of command arguments. It should be 5 with commandType");
        }
        try{
            Integer year = Integer.valueOf(command.get(1));
            Integer raceNum = Integer.valueOf(command.get(3));
            Double multiplier = Double.parseDouble(command.get(4));
            if(db.containsRace(year, raceNum))throw new CommandException("Race already exists");

        } catch (NumberFormatException e){
            throw new CommandException("Either the year,race number or multiplier value couldn't be converted to a number");
        }
    }
    /**
     * Makes a new race
     */
    public void execute(List<String> command, CommandExecutorImpl.ProgramState programState){
        RaceDB db = RaceDB.getInstance();
        CommandType commandType = CommandType.valueOf(command.get(0));
        int year = Integer.parseInt(command.get(1));
        String grandPrix = command.get(2);
        Integer RaceNum = Integer.valueOf(command.get(3));
        Double multiplier = Double.parseDouble(command.get(4));

        // Execute RACE command
        Race race = new Race(grandPrix,multiplier);
        RaceSeason raceSeason = db.getYear(year);
        raceSeason.addRace(RaceNum, race);

        programState.setActiveYear(year);
        programState.setActiveRaceNum(RaceNum);
        programState.setPrevCommand(commandType);
    }
}
