package hu.alerant.history.commands;

import hu.alerant.history.PointSystems;
import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.Query;
import hu.alerant.history.inputHandling.CommandExecutorImpl;

import java.util.List;

public class PointCommand implements Command {

    /**
     * Checks all the requirements before using the execute method. Throws errors with messages to handle it
     * in the CommandExecutorImpl
     * @param command
     * @throws CommandException message explains what was the problem in the command
     */
    public void validateCommand(List<String> command, CommandExecutorImpl.ProgramState programState) throws CommandException  {
        CommandType prevCommand = programState.getPrevCommand();
        StringBuilder str = new StringBuilder();
        if(CommandType.QUERY != prevCommand){
            str.append("Required previous command QUERY was not met. Previous command: ");
            str.append(prevCommand);
            throw new CommandException(str.toString());
        }
        if(command.size() != 2){
            throw new CommandException("Wrong number of command arguments. It should be 2 with commandType");
        }
        try{
            PointSystems pointSystem = PointSystems.valueOf(command.get(1));
        } catch (IllegalArgumentException e){
            throw new CommandException("There is no matching pointSystem with the given value");
        }
    }
    /**
     * With the provided point system, executes calculating the earned points
     */
    public void execute(List<String> command, CommandExecutorImpl.ProgramState programState) {
        CommandType commandType = CommandType.valueOf(command.get(0));

        PointSystems pointSystem = PointSystems.valueOf(command.get(1));

        boolean isFastestMatter = false;
        if(pointSystem == PointSystems.PRESENT) isFastestMatter = true;

        // Make Query with provided point system
        Query q = new Query(pointSystem, isFastestMatter);
        q.calculateScoreboard(programState);
        q.printScoreboard(programState);

        programState.setPrevCommand(commandType);
        programState.setQueryYear(-1);
        programState.setQueryEndRaceNum(-1);
    }
}
