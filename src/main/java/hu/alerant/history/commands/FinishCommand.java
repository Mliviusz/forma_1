package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.inputHandling.CommandExecutorImpl;

import java.util.Arrays;
import java.util.List;

public class FinishCommand implements Command {

    /**
     * Checks all the requirements before using the execute method. Throws errors with messages to handle it
     * in the CommandExecutorImpl
     * @param command
     * @throws CommandException message explains what was the problem in the command
     * @throws NumberFormatException
     */
    public void validateCommand(List<String> command, CommandExecutorImpl.ProgramState programState) throws CommandException  {
        CommandType prevCommand = programState.getPrevCommand();
        StringBuilder str = new StringBuilder();
        RaceDB db = RaceDB.getInstance();
        if(!Arrays.asList(CommandType.RACE, CommandType.RESULT, CommandType.FASTEST).contains(prevCommand)){
            str.append("Required previous command was not met. Previous command: ");
            str.append(prevCommand);
            throw new CommandException(str.toString());
        }
        if(command.size() != 1){
            throw new CommandException("Wrong number of command arguments. It should be 1 with commandType");
        }

        int size = db.activeRace(programState).driverAmount();
        if(size < 10){
            str.append("In Race [");
            str.append(db.activeRace(programState).getGrandPrix());
            str.append("] in year [");
            str.append(programState.getActiveYear());
            str.append("]: ");

            str.append("There are only [");
            str.append(size);
            str.append("] drivers administrated to the race, at least 10 is required to call FINISH command");
            throw new CommandException(str.toString());
        }
    }
    /**
     * executes FINISH command, closes activeRace and activeRaceNum by setting the to -1
     */
    public void execute(List<String> command, CommandExecutorImpl.ProgramState programState) {
        CommandType commandType = CommandType.valueOf(command.get(0));

        // Execute FINISH command
        programState.setActiveYear(-1);
        programState.setActiveRaceNum(-1);
        programState.setPrevCommand(commandType);
    }
}
