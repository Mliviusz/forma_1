package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.inputHandling.CommandExecutorImpl;

import java.util.Arrays;
import java.util.List;

public class FastestCommand implements Command {

    /**
     * Checks all the requirements before using the execute method. Throws errors with messages to handle it
     * in the CommandExecutorImpl
     * @param command
     * @throws CommandException message explains what was the problem in the command
     * @throws NumberFormatException
     */
    public void validateCommand(List<String> command, CommandExecutorImpl.ProgramState programState) throws CommandException  {
        CommandType prevCommand = programState.getPrevCommand();
        StringBuilder str = new StringBuilder();
        RaceDB db = RaceDB.getInstance();
        if(!Arrays.asList(CommandType.RACE, CommandType.RESULT, CommandType.FASTEST).contains(prevCommand)){
            str.append("Required previous command was not met. Previous command: ");
            str.append(prevCommand);
            throw new CommandException(str.toString());
        }
        if(command.size() != 3){
            throw new CommandException("Wrong number of command arguments. It should be 3 with commandType");
        }

        str.append("In Race [");
        str.append(db.activeRace(programState).getGrandPrix());
        str.append("] in year [");
        str.append(programState.getActiveYear());
        str.append("]: ");

        if (db.activeRace(programState).isThereFastest()) {
            str.append("[");
            str.append(db.activeRace(programState).getFastest());
            str.append("] is already declared as Fastest");
            throw new CommandException(str.toString());
        }

        String name = command.get(1);
        if(!db.activeYear(programState).containsDriver(name)){
            str.append("There is no driver with the name [");
            str.append(name);
            str.append("]");
            throw new CommandException(str.toString());
        }
    }
    /**
     * Adds who had the fastest lap on the race
     */
    public void execute(List<String> command, CommandExecutorImpl.ProgramState programState){
        RaceDB db = RaceDB.getInstance();
        CommandType commandType = CommandType.valueOf(command.get(0));
        String name = command.get(1);

        // Execute FINISH command
        db.activeRace(programState).setFastest(name);
        programState.setPrevCommand(commandType);
    }
}
