package hu.alerant.history.commands.exeptions;

public class CommandException extends Exception {

    public CommandException(String errorMessage) {
        super(errorMessage);
    }
}
