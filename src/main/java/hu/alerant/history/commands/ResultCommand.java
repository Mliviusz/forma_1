package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.Driver;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.inputHandling.CommandExecutorImpl;

import java.util.Arrays;
import java.util.List;

public class ResultCommand implements Command {

    /**
     * Checks all the requirements before using the execute method. Throws errors with messages to handle it
     * in the CommandExecutorImpl
     * @param command
     * @throws CommandException message explains what was the problem in the command
     * @throws NumberFormatException
     */
    public void validateCommand(List<String> command, CommandExecutorImpl.ProgramState programState) throws CommandException  {
        CommandType prevCommand = programState.getPrevCommand();
        StringBuilder str = new StringBuilder();
        RaceDB db = RaceDB.getInstance();
        if(!Arrays.asList(CommandType.RACE, CommandType.FASTEST, CommandType.RESULT).contains(prevCommand)){
            str.append("Required previous command was not met. Previous command: ");
            str.append(prevCommand);
            throw new CommandException(str.toString());
        }
        if(command.size() != 4){
            throw new CommandException("Wrong number of command arguments. It should be 4 with commandType");
        }
        try{
            Integer place = Integer.valueOf(command.get(1));
            if(db.activeRace(programState).containsDriver(place)){
                str.append("In Race [");
                str.append(db.activeRace(programState).getGrandPrix());
                str.append("] in year [");
                str.append(programState.getActiveYear());
                str.append("]: ");

                str.append("Placement is already taken by [");
                str.append(db.activeRace(programState).getDriver(place).getName());
                str.append("] from team [");
                str.append(db.activeRace(programState).getDriver(place).getTeam());
                str.append("]");
                throw new CommandException(str.toString());
            }
        } catch (NumberFormatException e){
            throw new CommandException("The place value couldn't be converted to a number");
        }
    }
    /**
     * Adds 1 placement and all of its information to the active race
     */
    public void execute(List<String> command, CommandExecutorImpl.ProgramState programState) {
        RaceDB db = RaceDB.getInstance();
        CommandType commandType = CommandType.valueOf(command.get(0));
        int place = Integer.parseInt(command.get(1));
        String name = command.get(2);
        String team = command.get(3);

        // Execute RESULT command

        Driver driver = db.activeYear(programState).getDriver(name, team);
        db.activeRace(programState).addDriver(place, driver);
        programState.setPrevCommand(commandType);
    }
}
