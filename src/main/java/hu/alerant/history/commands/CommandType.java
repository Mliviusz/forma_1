package hu.alerant.history.commands;

/** Enum which contains all the possible Commands int the program */
public enum CommandType {
    RACE,
    RESULT,
    FASTEST,
    FINISH,
    QUERY,
    POINT,
    EXIT;

    public Command createCommand() {
        switch (this) {
            case RACE:
                return new RaceCommand();
            case RESULT:
                return new ResultCommand();
            case FASTEST:
                return new FastestCommand();
            case FINISH:
                return new FinishCommand();
            case QUERY:
                return new QueryCommand();
            case POINT:
                return new PointCommand();
            default:
                return null;
        }
    }
}