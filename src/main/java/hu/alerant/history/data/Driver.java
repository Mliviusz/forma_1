package hu.alerant.history.data;

/** Drivers data is stored here */
public class Driver {

    private String name;

    /** Since we are not ranking teams, it is only a bonus information
     * If we extended the program to also rank teams, then we would use Objects called Team instead of String
     */
    private String team;

    public Driver(String name, String team){
        this.name = name;
        this.team = team;
    }

    public String getName() {
        return name;
    }

    public String getTeam() {
        return team;
    }
}
