package hu.alerant.history.data;

import hu.alerant.history.PointSystems;
import hu.alerant.history.inputHandling.CommandExecutorImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Query {
    private PointSystems pointSystem;
    private boolean isFastestMatter;

    private Map<String,Double> scoreboard;

    public Query(PointSystems pointSystem, boolean isFastestMatter) {
        this.pointSystem = pointSystem;
        this.isFastestMatter = isFastestMatter;
        scoreboard = new HashMap<>();
    }

    /**
     * Creates a scoreboard with all of the competitors and 0 points, and loops through the races to add points to them
     */
    public void calculateScoreboard(CommandExecutorImpl.ProgramState programState){

        RaceDB db = RaceDB.getInstance();
        RaceSeason queriedYear =  db.queriedYear(programState);
        int endRaceNum = programState.getQueryEndRaceNum();

        // Creates a scoreboard for the competitors
        for (String name : queriedYear.driversKeySet()) {
            scoreboard.put(name, 0.);
        }

        if(endRaceNum == -1) {
            endRaceNum = queriedYear.highestEndRaceNum();
        }

        for(int i=1; i<=endRaceNum; i++){
            // Skips if data is inconsistent and there is a gap between races and raceNums
            if(!queriedYear.containsRace(i)) { continue; }
            calculateRace(i, programState);
        }
    }


    /** Adds the acquired points from this race considering pointsystem, race multiplier and isFastestMatter
     * @param RaceNum tells which specific race's result we will calculate and add to the scoreBoard
     */
    private void calculateRace(int RaceNum, CommandExecutorImpl.ProgramState programState){

        Race race = RaceDB.getInstance().queriedYear(programState).getRace(RaceNum);

        List<Double> points = pointSystem.getPoints();
        for(int placement=1; placement<=points.size(); ++placement){
            // Skips if data is inconsistent and there is a gap between drivers and placements
            if(!race.containsDriver(placement)){ continue; }

            String driverName = race.getDriver(placement).getName();
            double newPoint = scoreboard.get(driverName) + points.get(placement-1) * race.getMultiplier();

            if(isFastestMatter && driverName.equals(race.getFastest())){
                newPoint += race.getMultiplier();
            }
            scoreboard.replace(driverName, newPoint);
        }
    }

    public void printScoreboard(CommandExecutorImpl.ProgramState programState){

        RaceDB db = RaceDB.getInstance();

        StringBuilder str = new StringBuilder();
        str.append("\nThe result of the ");
        str.append(programState.getQueryYear());

        if(programState.getQueryEndRaceNum() == -1) {
            str.append(" world cup:\n");
        } else {
            str.append(" world cup after the ");
            str.append(programState.getQueryEndRaceNum());
            str.append(". race:\n");
        }
        System.out.println(str.toString());

        // This just sorts the Driver by their total points and prints out them with all the information
        int counter = 1;
        RaceSeason season = db.queriedYear(programState);
        List<Map.Entry<String, Double>> toSort = new ArrayList<>(scoreboard.entrySet());
        toSort.sort((k1, k2) -> -k1.getValue().compareTo(k2.getValue()));
        for (Map.Entry<String, Double> k : toSort) {
            System.out.printf("%d. %s, %s : %.1f points\n",counter, k.getKey(), season.getDriver(k.getKey()).getTeam(), k.getValue());
            counter++;
        }
    }
}
