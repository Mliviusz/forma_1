package hu.alerant.history.data;

import java.util.HashMap;
import java.util.Map;

public class Race {

    private String grandPrix;

    /** Point multiplier when calculating scoreboard */
    private Double multiplier;

    /** The name of the fastest lap's owner */
    private String fastest;

    /** Placement -> Driver */
    Map<Integer, Driver> drivers;

    public Race(String grandPrix,Double multiplier){
        this.grandPrix = grandPrix;
        this.multiplier = multiplier;
        drivers = new HashMap<>();
    }

    public Driver getDriver(Integer place) { return drivers.get(place); }

    public boolean containsDriver(Integer place) { return drivers.containsKey(place); }

    public void addDriver(Integer place, Driver driver){
        drivers.put(place, driver);
    }

    public int driverAmount() { return drivers.size(); }

    public String getGrandPrix() { return grandPrix; }

    public void setFastest(String fastest) {
        this.fastest = fastest;
    }

    public boolean isThereFastest() { return fastest != null; }

    public String getFastest() { return fastest; }

    public Double getMultiplier() { return multiplier; }
}
