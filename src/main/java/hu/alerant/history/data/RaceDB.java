package hu.alerant.history.data;

import hu.alerant.history.inputHandling.CommandExecutorImpl;

import java.util.HashMap;
import java.util.Map;

public class RaceDB {

    private Map<Integer, RaceSeason> years = new HashMap<>();

    private RaceDB(){ }

    private static RaceDB db;

    public static RaceDB getInstance(){
        if(db == null)
            db = new RaceDB();
        return db;
    }

    public RaceSeason getYear(int year){
        RaceSeason raceSeason;
        if (!containsYear(year)) {
            raceSeason = new RaceSeason();
            years.put(year, raceSeason);
        } else {
            raceSeason = years.get(year);
        }
        return raceSeason;
    }

    public boolean containsYear(int year) {
        return years.containsKey(year);
    }

    public boolean containsRace(int year, int raceNum){
        return containsYear(year) && getYear(year).containsRace(raceNum);
    }

    public RaceSeason activeYear(CommandExecutorImpl.ProgramState programState){
        return getYear(programState.getActiveYear());
    }

    public Race activeRace(CommandExecutorImpl.ProgramState programState){
        return getYear(programState.getActiveYear()).getRace(programState.getActiveRaceNum());
    }

    public RaceSeason queriedYear(CommandExecutorImpl.ProgramState programState){
        return getYear(programState.getQueryYear());
    }

    public void clear() { years.clear(); }
}
