package hu.alerant.history.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static java.util.Collections.max;

/**
 * Contains all the information of a year, including a list of competitors, races and methods for them
 */
public class RaceSeason {

    /** Racer's Name -> Racer's Competitor Object */
    private Map<String, Driver> drivers;

    /** Race's number in the year -> Race */
    private Map<Integer, Race> races;

    public RaceSeason(){
        drivers = new HashMap<>();
        races = new HashMap<>();
    }

    public void addRace(Integer raceNum, Race race) {
        races.put(raceNum, race);
    }

    public Race getRace(Integer raceNum) { return races.get(raceNum); }
    public boolean containsRace(Integer raceNum) { return races.containsKey(raceNum); }
    public int highestEndRaceNum() { return max(races.keySet()); }

    /**
     * Check whether a competitor is in the competiorlist, if not create a new and adds it to the list
     * @param name Name of the competitor
     * @param team Team of the competitor
     * @return Comptetiro Object
     */
    public Driver getDriver(String name, String team){
        Driver driver;
        if (!containsDriver(name)) {
            driver = new Driver(name, team);
            drivers.put(name, driver);
        } else {
            driver = drivers.get(name);
        }
        return driver;
    }

    public Driver getDriver(String name){
        Driver driver;
        if (!containsDriver(name)) {
            driver = new Driver(name, "No information");
            drivers.put(name, driver);
        } else {
            driver = drivers.get(name);
        }
        return driver;
    }

    public boolean containsDriver(String name) { return drivers.containsKey(name); }

    public Set<String> driversKeySet() { return drivers.keySet(); }
}
