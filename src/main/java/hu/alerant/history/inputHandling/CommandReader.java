package hu.alerant.history.inputHandling;

import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Parancssorból vagy fájlból beolvasást végző utility osztály.
 */
public class CommandReader {

    /**
     * A parancssoron érkező sor beolvasását és feltördelését végzi ; karakterek mentén.
     * @return A feltördelt parancs: parancsszó és argumentumai. Lehet egy üres lista is.
     * @throws IOException Amennyiben sikertelen volt a parancssorból beolvasás, kivételt dob.
     */
    public static List<String> read(CommandExecutorImpl.ProgramState programState) throws IOException{
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        OptionValidator validator = new OptionValidator();

        return validator.reformat(splitArguments(console.readLine()), programState);
    }

    /**
     * A fájból történő beolvasást végzi el, hasonlóan a paraméter nélküli read függvényhez.
     * @param filename A beolvasandó fájl neve.
     * @return A beolvasott sorok, melyben a parancs fel van tördelve.
     * @throws IOException Amennyiben sikertelen volt a fájlból történő beolvasás, kivételt dob.
     */
    public static List<List<String>> read(String filename) throws IOException{
        try(BufferedReader reader = new BufferedReader(new FileReader(filename))){
            List<List<String>> lines = new LinkedList<>();
            String line = reader.readLine();
            while(line != null){
                lines.add(readLine(line));
                line = reader.readLine();
            }
            return lines;
        }
    }

    private static List<String> readLine(String line){
        String[] command = line.split(";");
        return Arrays.asList(command);
    }

    /**
     *
     * @param line input string from conlose
     * @return a list of arguments where quotation marked elements stays to together lie "British Grand Prix"
     */
    protected static String[] splitArguments(String line){
        String regex = " (?!(\\w))| (?=[0-9])| (?!.*\")";
        return line.split(regex);
    }

}
