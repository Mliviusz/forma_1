package hu.alerant.history.inputHandling;

import org.apache.commons.cli.*;

import java.util.ArrayList;
import java.util.List;

public class OptionValidator {

    private final Option ARG_HELP = new Option("help", false,"Shows the usable Commands with their arguments");

    private final Option CMD_RACE = new Option("RACE", false,"Shows the usable Commands with their arguments");
    private final Option RACE_YEAR = new Option("year",true,"Year of the race");
    private final Option RACE_NAME = new Option("racename",true,"Name of the Grand Prix");
    private final Option RACE_NUMBER = new Option("racenumber",true,"Gives which/ Nth race it is in the year");
    private final Option RACE_POINT_MULTI = new Option("pointmulti",true,"Gives the point multiplier corresponding the race");

    private final Option CMD_RESULT = new Option("RESULT", false,"Shows the usable Commands with their arguments");
    private final Option RESULT_POSITION = new Option("position", true,"Gives the final position of the driver in the race");
    private final Option RESULT_DRIVER = new Option("driver", true,"Gives the driver's name ");
    private final Option RESULT_CONSTRUCTOR = new Option("constructor", true,"Gives the driver's constructor");

    private final Option CMD_FASTEST = new Option("FASTEST", false,"Shows the usable Commands with their arguments");
    private final Option CMD_FINISH = new Option("FINISH", false,"Shows the usable Commands with their arguments");
    private final Option CMD_QUERY = new Option("QUERY", true,"Shows the usable Commands with their arguments");
    private final Option CMD_POINT = new Option("POINT", true,"Shows the usable Commands with their arguments");
    private final Option CMD_EXIT = new Option("EXIT", false,"Shows the usable Commands with their arguments");

    public OptionValidator() {}

    public List<String> reformat(String[] line, CommandExecutorImpl.ProgramState programState) {
        CommandLineParser clp = new DefaultParser();
        Options options = new Options();
        List<String> commandList = new ArrayList<>();

        options.addOption(ARG_HELP);

        options.addOption(CMD_RACE);
        options.addOption(RACE_YEAR);
        options.addOption(RACE_NAME);
        options.addOption(RACE_NUMBER);
        options.addOption(RACE_POINT_MULTI);

        options.addOption(CMD_RESULT);
        options.addOption(RESULT_POSITION);
        options.addOption(RESULT_DRIVER);
        options.addOption(RESULT_CONSTRUCTOR);

        options.addOption(CMD_FASTEST);
        options.addOption(CMD_FINISH);
        options.addOption(CMD_QUERY);
        options.addOption(CMD_POINT);
        options.addOption(CMD_EXIT);

        try{
            CommandLine cl = clp.parse(options, line);

            if(cl.hasOption(ARG_HELP.getOpt())){
                if(programState.getActiveYear() != -1){
                    System.out.println("Usable commands with argument examples:\n");
                    System.out.println("-RESULT -position 1 -driver \"Lewis Hamilton\" -constructor \"Mercedes\"");
                    System.out.println("-FASTEST -driver \"Lewis Hamilton\" -constructor \"Red Bull\"");
                    System.out.println("-FINISH");
                }else if(programState.getQueryYear() != -1){
                    System.out.println("Usable commands with argument examples:\n");
                    System.out.println("-POINT CLASSIC (/MODERN/NEW/PRESENT)");
                }else{
                    System.out.println("Usable commands with argument examples:\n");
                    System.out.println("-RACE -year 2020 -racename \"British Grand Prix\" -racenumber 5 -pointmulti 1");
                    System.out.println("-QUERY 2020   or  -QUERY 2020 -racenumber 5");
                }
                System.out.println("-EXIT");
            }else if(cl.hasOption(CMD_RACE.getOpt())){
                commandList.add("RACE");
                commandList.add(cl.getOptionValue(RACE_YEAR.getOpt()));
                commandList.add(cl.getOptionValue(RACE_NAME.getOpt()));
                commandList.add(cl.getOptionValue(RACE_NUMBER.getOpt()));
                commandList.add(cl.getOptionValue(RACE_POINT_MULTI.getOpt()));
            }else if(cl.hasOption(CMD_RESULT.getOpt())){
                commandList.add("RESULT");
                commandList.add(cl.getOptionValue(RESULT_POSITION.getOpt()));
                commandList.add(cl.getOptionValue(RESULT_DRIVER.getOpt()));
                commandList.add(cl.getOptionValue(RESULT_CONSTRUCTOR.getOpt()));
            }else if(cl.hasOption(CMD_FASTEST.getOpt())){
                commandList.add("FASTEST");
                commandList.add(cl.getOptionValue(RESULT_DRIVER.getOpt()));
                commandList.add(cl.getOptionValue(RESULT_CONSTRUCTOR.getOpt()));
            }else if(cl.hasOption(CMD_FINISH.getOpt())){
                commandList.add("FINISH");
            }else if(cl.hasOption(CMD_QUERY.getOpt())){
                commandList.add("QUERY");
                commandList.add(cl.getOptionValue(CMD_QUERY.getOpt()));
                if(cl.hasOption(RACE_NUMBER.getOpt())){
                    commandList.add(cl.getOptionValue(RACE_NUMBER.getOpt()));
                }
            }else if(cl.hasOption(CMD_POINT.getOpt())){
                commandList.add("POINT");
                commandList.add(cl.getOptionValue(CMD_POINT.getOpt()));
            }else if(cl.hasOption(CMD_EXIT.getOpt())){
                commandList.add("EXIT");
            }else{
                System.out.println("Please provide a command, use -help if needed");
            }
        } catch (ParseException e){
            commandList.clear();
            System.err.println("There was an error during parsing, most likely not enough data or invalid argument");
        }
        return commandList;
    }
}
