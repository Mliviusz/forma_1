package hu.alerant.history.inputHandling;

import java.util.List;

/**
 * Az implementálandó parancsfeldolgozó interfész.
 */
public interface CommandExecutor {
    /**
     * A parancs végrehajtásáért felelős függvény.
     * @param command A parancs: az első elem a parancsszó, melytől függ a végrehajtás, a többi elem argumentum.
     * @return Az alkalmazás futása folytatódjon-e, vagy ki kell lépni.
     */
    boolean runCommand(List<String> command);
}
