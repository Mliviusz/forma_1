package hu.alerant.history.inputHandling;

import java.util.List;

/**
 * Példa, kilépést vezérlő és echot végző, dummy parancsfeldolgozó implementáció.
 */
public class EchoExecutor implements CommandExecutor{
    /**
     * A megkapott parancsot feltördelve, soronként kiírja a konzolra,
     * majd megnézi, az EXIT parancsszó érkezett-e - amennyiben igen, kilépteti az alkalmazást.
     * @param command A parancs: az első elem a parancsszó, melytől függ a végrehajtás, a többi elem argumentum.
     * @return Ha EXIT a parancsszó, akkor hamis, egyébként igaz.
     */
    @Override
    public boolean runCommand(List<String> command) {
        command.forEach(System.out::println);
        return !(command.size() > 0 && command.get(0).equals("EXIT"));
    }
}
