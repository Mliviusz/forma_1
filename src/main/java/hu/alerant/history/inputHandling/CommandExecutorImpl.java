package hu.alerant.history.inputHandling;

import hu.alerant.history.commands.*;
import hu.alerant.history.commands.exeptions.CommandException;

import java.util.List;

public class CommandExecutorImpl implements CommandExecutor{

    public ProgramState programState;

    public class ProgramState {
         private Integer activeYear;
         private Integer activeRaceNum;
         private Integer queryYear;
         private Integer queryEndRaceNum;
         private CommandType prevCommand;

         public ProgramState() {
             activeYear = -1;
             activeRaceNum = -1;
             queryYear = -1;
             queryEndRaceNum = -1;
             prevCommand = CommandType.FINISH;
         }
         public Integer getActiveYear() { return activeYear; }
         public void setActiveYear(Integer activeYear) { this.activeYear = activeYear; }
         public Integer getActiveRaceNum() { return activeRaceNum; }
         public void setActiveRaceNum(Integer activeRaceNum) { this.activeRaceNum = activeRaceNum; }
         public Integer getQueryYear() { return queryYear; }
         public void setQueryYear(Integer queryYear) { this.queryYear = queryYear; }
         public Integer getQueryEndRaceNum() { return queryEndRaceNum; }
         public void setQueryEndRaceNum(Integer queryEndRaceNum) { this.queryEndRaceNum = queryEndRaceNum; }
         public CommandType getPrevCommand() { return prevCommand; }
         public void setPrevCommand(CommandType prevCommand) { this.prevCommand = prevCommand; }
    }

    public CommandExecutorImpl() {
         programState = new ProgramState();
    }

    /** Processes which command was provided and send data for validation and execution
     */
    @Override
    public boolean runCommand(List<String> command) {
        try{
            if(command.get(0).equals("")) return true;
            CommandType commandType = CommandType.valueOf(command.get(0));
            Command cmd = commandType.createCommand();
            if(cmd == null){
                return !(command.size() > 0 && command.get(0).equals("EXIT"));
            }
            cmd.validateCommand(command, programState);
            cmd.execute(command, programState);

        } catch (IllegalArgumentException e){
            System.err.printf("\nERROR: Command %s was invalid and had to be skipped\n", command);
        } catch (CommandException e) {

            StringBuilder errorOutput = new StringBuilder();
            errorOutput.append("\nERROR: Command ");
            errorOutput.append(command.toString());
            errorOutput.append(" was invalid and had to be skipped\n");
            errorOutput.append(e.getMessage());
            errorOutput.append("\n");

            System.err.print(errorOutput.toString());
        }
        return !(command.size() > 0 && command.get(0).equals("EXIT"));
    }
}
