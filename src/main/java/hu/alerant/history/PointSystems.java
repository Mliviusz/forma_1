package hu.alerant.history;

import java.util.Arrays;
import java.util.List;

/** Enum for all the possible point systems, with their respecting pointtable */
public enum PointSystems {
    CLASSIC(10., 6., 4., 3., 2., 1.),
    MODERN(10., 8., 6., 5., 4., 3., 2., 1.),
    NEW(25., 18., 15., 12., 10., 8., 6., 4., 2., 1.),
    PRESENT(25., 18., 15., 12., 10., 8., 6., 4., 2., 1.);

    private final List<Double> points;

    PointSystems(Double ... points){
        this.points = Arrays.asList(points);
    }

    public List<Double> getPoints(){
        return points;
    }
}
