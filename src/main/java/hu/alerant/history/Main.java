package hu.alerant.history;

import hu.alerant.history.inputHandling.CommandExecutorImpl;
import hu.alerant.history.inputHandling.CommandReader;
import org.apache.commons.cli.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * Main osztály, mely az alkalmazás indítását végzi.
 */
@SpringBootApplication
public class Main {

    private static final Option ARG_HELP = new Option("help", false,"Shows the usable arguments");
    private static final Option ARG_FILE = new Option("f", "file",true,"Adds file argument");
    private static final Option ARG_CMD = new Option("cmd", false,"Runs the program from command line");

    /**
     * Main függvény, az alkalmazás indítását végzi - ciklikusan beolvastat egy parancsot, majd ezt továbbítja
     * a feldolgozónak.
     * @param args Parancssori argumentumok - nem kerülnek felhasználásra.
     * @throws IOException Amennyiben a parancssorból vagy a fájlból olvasás hibás, kivétellel elszáll a program.
     */
    public static void main(String[] args) throws IOException {
        CommandLineParser clp = new DefaultParser();

        Options options = new Options();
        options.addOption(ARG_CMD);
        options.addOption(ARG_FILE);
        options.addOption(ARG_HELP);

        try{
            CommandLine cl = clp.parse(options, args);

            CommandExecutorImpl exec = new CommandExecutorImpl();
            // fájlból olvasunk
            if(cl.hasOption(ARG_FILE.getOpt())){
                CommandReader.read(cl.getOptionValue("f")).forEach(exec::runCommand);
            }

            // parancssorból olvasunk
            else if (cl.hasOption(ARG_CMD.getOpt())){
                while(exec.runCommand(CommandReader.read(exec.programState)));
            }
            else if (cl.hasOption(ARG_HELP.getOpt())){
                System.out.println("Available [options]:");
                System.out.println("-f filename ->  for reading input from a file");
                System.out.println("-cmd        ->  for reading input from console");
            }else{
                System.exit(-1);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SpringApplication.run(Main.class, args);
    }
}
