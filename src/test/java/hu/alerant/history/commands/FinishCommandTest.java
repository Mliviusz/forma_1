package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.Driver;
import hu.alerant.history.data.Race;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.inputHandling.CommandExecutorImpl;
import org.junit.Before;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FinishCommandTest {

    private CommandExecutorImpl.ProgramState programStateMock;

    @Before
    public void clear_setupState_AddRace() {
        RaceDB.getInstance().clear();
        RaceDB.getInstance().getYear(2020).addRace(5,new Race("British Grand Prix", 1.));
        programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        setRaceStateMock(2020, 5);
        Driver lewis = new Driver("Lewis Hamilton", "Red Bull");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(1,lewis);
        RaceDB.getInstance().activeRace(programStateMock).addDriver(2,new Driver("Marton Krooger", "Mercedes"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(3,new Driver("Marton Krooger", "Merci"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(4,new Driver("Martn Kroner", "Red Bully"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(5,new Driver("Marssaason Goger", "Red Duck"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(6,new Driver("Lewisi Hamili", "Yellow Bull"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(7,new Driver("Lais Hamiliteon", "Red Kitten"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(8,new Driver("Ewis Amilton", "Meedaios"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(9,new Driver("Leis Clark Hamiton", "Mecedenes"));
    }

    private void addTenthDriver() {
        RaceDB.getInstance().activeRace(programStateMock).addDriver(10,new Driver("Arnold Swarcanegger", "Terminator"));
    }

    public void setRaceStateMock(int year, int raceNum){
        when(programStateMock.getActiveYear()).thenReturn(year);
        when(programStateMock.getActiveRaceNum()).thenReturn(raceNum);
    }

    @org.junit.Test
    public void validateCommand_pass() throws CommandException {

        addTenthDriver();
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE).thenReturn(CommandType.FASTEST).thenReturn(CommandType.RESULT);

        List<String> input  = Arrays.asList("FINISH");

        Command finishCommand = new FinishCommand();

        finishCommand.validateCommand(input, programStateMock);
        finishCommand.validateCommand(input, programStateMock);
        finishCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test
    public void execute_pass(){

        addTenthDriver();
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE).thenReturn(CommandType.FASTEST).thenReturn(CommandType.RESULT);

        List<String> input  = Arrays.asList("FINISH");

        Command finishCommand = new FinishCommand();

        finishCommand.execute(input, programStateMock);
        finishCommand.execute(input, programStateMock);
        finishCommand.execute(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_finish() throws CommandException {

        addTenthDriver();
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("FINISH");

        Command resultCommand = new FinishCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_query() throws CommandException {

        addTenthDriver();
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.QUERY);

        List<String> input  = Arrays.asList("FINISH");

        Command resultCommand = new FinishCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_point() throws CommandException {

        addTenthDriver();
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.POINT);

        List<String> input  = Arrays.asList("FINISH");

        Command resultCommand = new FinishCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_moreArgument() throws CommandException {

        addTenthDriver();
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE);

        List<String> input  = Arrays.asList("FINISH", "error");

        Command resultCommand = new FinishCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_lessDriverThanRequired() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RESULT);

        List<String> input  = Arrays.asList("FINISH");

        Command resultCommand = new FinishCommand();

        resultCommand.validateCommand(input, programStateMock);
    }
}
