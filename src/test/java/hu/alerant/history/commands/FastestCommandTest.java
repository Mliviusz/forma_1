package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.Driver;
import hu.alerant.history.data.Race;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.inputHandling.CommandExecutorImpl;
import org.junit.Before;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FastestCommandTest {

    private CommandExecutorImpl.ProgramState programStateMock;

    @Before
    public void clear_setupState_AddRace() {
        RaceDB.getInstance().clear();
        RaceDB.getInstance().getYear(2020).addRace(5,new Race("British Grand Prix", 1.));
        programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        setRaceStateMock(2020, 5);
        Driver lewis = new Driver("Lewis Hamilton", "Red Bull");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(1,lewis);
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Lewis Hamilton","Red Bull");
    }

    public void setRaceStateMock(int year, int raceNum){
        when(programStateMock.getActiveYear()).thenReturn(year);
        when(programStateMock.getActiveRaceNum()).thenReturn(raceNum);
    }

    @org.junit.Test
    public void validateCommand_pass() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE).thenReturn(CommandType.RESULT);

        List<String> input  = Arrays.asList("FASTEST", "Lewis Hamilton", "Red Bull");

        Command fastestCommand = new FastestCommand();

        fastestCommand.validateCommand(input, programStateMock);
        fastestCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test
    public void execute_pass(){

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE).thenReturn(CommandType.RESULT);

        List<String> input  = Arrays.asList("FASTEST", "Lewis Hamilton", "Red Bull");

        Command fastestCommand = new FastestCommand();

        fastestCommand.execute(input, programStateMock);
        fastestCommand.execute(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_fastestAlreadyGiven() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE);
        RaceDB.getInstance().activeRace(programStateMock).setFastest("Marton Brooklin");
        List<String> input  = Arrays.asList("FASTEST", "Lewis Hamilton", "Red Bull");

        Command resultCommand = new FastestCommand();

        resultCommand.validateCommand(input, programStateMock);
    }


    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_finish() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("FASTEST", "Lewis Hamilton", "Red Bull");

        Command resultCommand = new FastestCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_query() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.QUERY);

        List<String> input  = Arrays.asList("FASTEST", "Lewis Hamilton", "Red Bull");

        Command resultCommand = new FastestCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_point() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.POINT);

        List<String> input  = Arrays.asList("FASTEST", "Lewis Hamilton", "Red Bull");

        Command resultCommand = new FastestCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_lessArgument() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE);

        List<String> input  = Arrays.asList("FASTEST", "Lewis Hamilton");

        Command resultCommand = new FastestCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_moreArgument() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE);

        List<String> input  = Arrays.asList("FASTEST", "Lewis Hamilton", "Red Bull", "valami");

        Command resultCommand = new FastestCommand();

        resultCommand.validateCommand(input, programStateMock);
    }
}
