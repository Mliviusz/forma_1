package hu.alerant.history.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class CommandTypeTest {

    @org.junit.Test
    public void createCommand(){
        assertEquals(RaceCommand.class, (CommandType.RACE.createCommand()).getClass());
        assertEquals(ResultCommand.class, (CommandType.RESULT.createCommand()).getClass());
        assertEquals(FastestCommand.class, (CommandType.FASTEST.createCommand()).getClass());
        assertEquals(FinishCommand.class, (CommandType.FINISH.createCommand()).getClass());
        assertEquals(QueryCommand.class, (CommandType.QUERY.createCommand()).getClass());
        assertEquals(PointCommand.class, (CommandType.POINT.createCommand()).getClass());
        assertNull(CommandType.EXIT.createCommand());
    }
}
