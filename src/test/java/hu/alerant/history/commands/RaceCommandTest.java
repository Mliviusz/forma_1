package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.Race;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.inputHandling.CommandExecutorImpl;
import org.junit.Before;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RaceCommandTest {

    @Before
    public void clear() {
        RaceDB.getInstance().clear();
    }

    @org.junit.Test
    public void validateCommand_pass() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH).thenReturn(CommandType.POINT);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5", "1");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test
    public void execute_pass(){

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH).thenReturn(CommandType.POINT);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5", "1");

        Command raceCommand = new RaceCommand();

        raceCommand.execute(input, programStateMock);
        raceCommand.execute(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_race() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5", "1");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_result() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RESULT);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5", "1");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_fastest() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FASTEST);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5", "1");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_query() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.QUERY);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5", "1");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_lessArgument() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_moreArgument() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5", "1", "valami");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_notInteger1() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("RACE", "2020h", "British Grand Prix", "5", "1");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_notInteger2() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5.6", "1");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_notDouble() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5", "k");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_raceNumAlreadyTakenInTheYear() throws CommandException {

        RaceDB.getInstance().getYear(2020).addRace(5,new Race("British Grand Prix", 1.));

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("RACE", "2020", "British Grand Prix", "5", "1");

        Command raceCommand = new RaceCommand();

        raceCommand.validateCommand(input, programStateMock);

    }
}
