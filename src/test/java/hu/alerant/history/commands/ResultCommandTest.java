package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.Driver;
import hu.alerant.history.data.Race;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.inputHandling.CommandExecutorImpl;
import org.junit.Before;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ResultCommandTest {

    @Before
    public void clearAndAddRace() {
        RaceDB.getInstance().clear();
        RaceDB.getInstance().getYear(2020).addRace(5,new Race("British Grand Prix", 1.));
    }

    public void setRaceStateMock(CommandExecutorImpl.ProgramState programStateMock, int year, int raceNum){
        when(programStateMock.getActiveYear()).thenReturn(year);
        when(programStateMock.getActiveRaceNum()).thenReturn(raceNum);
    }

    @org.junit.Test
    public void validateCommand_pass() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE).thenReturn(CommandType.FASTEST).thenReturn(CommandType.RESULT);
        setRaceStateMock(programStateMock, 2020, 5);

        List<String> input  = Arrays.asList("RESULT", "1", "Lewis Hamilton", "Mercedes");

        Command resultCommand = new ResultCommand();

        resultCommand.validateCommand(input, programStateMock);
        resultCommand.validateCommand(input, programStateMock);
        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test
    public void execute_pass(){

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE).thenReturn(CommandType.FASTEST).thenReturn(CommandType.RESULT);
        setRaceStateMock(programStateMock, 2020, 5);

        List<String> input  = Arrays.asList("RESULT", "1", "Lewis Hamilton", "Mercedes");
        List<String> input2  = Arrays.asList("RESULT", "2", "Arthas the young", "Azeroth");
        List<String> input3  = Arrays.asList("RESULT", "3", "Arnold S", "Terminators");

        Command resultCommand = new ResultCommand();

        resultCommand.execute(input, programStateMock);
        resultCommand.execute(input2, programStateMock);
        resultCommand.execute(input3, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_finish() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);
        setRaceStateMock(programStateMock, 2020, 5);

        List<String> input  = Arrays.asList("RESULT", "1", "Lewis Hamilton", "Mercedes");

        Command resultCommand = new ResultCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_query() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.QUERY);
        setRaceStateMock(programStateMock, 2020, 5);

        List<String> input  = Arrays.asList("RESULT", "1", "Lewis Hamilton", "Mercedes");

        Command resultCommand = new ResultCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_point() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.POINT);
        setRaceStateMock(programStateMock, 2020, 5);

        List<String> input  = Arrays.asList("RESULT", "1", "Lewis Hamilton", "Mercedes");

        Command resultCommand = new ResultCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_lessArgument() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE);
        setRaceStateMock(programStateMock, 2020, 5);

        List<String> input  = Arrays.asList("RESULT", "1", "Lewis Hamilton");

        Command resultCommand = new ResultCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_moreArgument() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE);
        setRaceStateMock(programStateMock, 2020, 5);

        List<String> input  = Arrays.asList("RESULT", "1", "Lewis Hamilton", "Mercedes", "hiba");

        Command resultCommand = new ResultCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_notInteger() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE);
        setRaceStateMock(programStateMock, 2020, 5);

        List<String> input1  = Arrays.asList("RESULT", "1.2", "Lewis Hamilton", "Mercedes");
        List<String> input2  = Arrays.asList("RESULT", "h", "Lewis Hamilton", "Mercedes");

        Command resultCommand = new ResultCommand();

        resultCommand.validateCommand(input1, programStateMock);
        resultCommand.validateCommand(input2, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_positionAlreadyTakenByAnotherDriver() throws CommandException {

        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);
        setRaceStateMock(programStateMock, 2020, 5);
        RaceDB.getInstance().activeRace(programStateMock).addDriver(1,new Driver("Marton Brooklin", "Red Bull"));

        List<String> input  = Arrays.asList("RESULT", "1", "Lewis Hamilton", "Mercedes");

        Command resultCommand = new ResultCommand();

        resultCommand.validateCommand(input, programStateMock);
    }
}
