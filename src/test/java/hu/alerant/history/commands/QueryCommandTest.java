package hu.alerant.history.commands;

import hu.alerant.history.commands.exeptions.CommandException;
import hu.alerant.history.data.Driver;
import hu.alerant.history.data.Race;
import hu.alerant.history.data.RaceDB;
import hu.alerant.history.inputHandling.CommandExecutorImpl;
import org.junit.Before;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class QueryCommandTest {

    private CommandExecutorImpl.ProgramState programStateMock;

    @Before
    public void clear_setupState_AddRace() {
        RaceDB.getInstance().clear();
        RaceDB.getInstance().getYear(2020).addRace(5,new Race("British Grand Prix", 1.));
        programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        setRaceStateMock(2020, 5);

        Driver lewis = new Driver("Lewis Hamilton", "Red Bull");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(1,lewis);
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Lewis Hamilton","Red Bull");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(2,new Driver("Marton Krooger", "Mercedes"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Marton Krooger", "Mercedes");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(3,new Driver("Marton Krooger", "Merci"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Marton Krooger", "Merci");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(4,new Driver("Martn Kroner", "Red Bully"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Martn Kroner", "Red Bully");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(5,new Driver("Marssaason Goger", "Red Duck"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Marssaason Goger", "Red Duck");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(6,new Driver("Lewisi Hamili", "Yellow Bull"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Lewisi Hamili", "Yellow Bull");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(7,new Driver("Lais Hamiliteon", "Red Kitten"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Lais Hamiliteon", "Red Kitten");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(8,new Driver("Ewis Amilton", "Meedaios"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Ewis Amilton", "Meedaios");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(9,new Driver("Leis Clark Hamiton", "Mecedenes"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Leis Clark Hamiton", "Mecedenes");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(10,new Driver("Arnold Swarcanegger", "Terminator"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Arnold Swarcanegger", "Terminator");


        RaceDB.getInstance().getYear(2020).addRace(2,new Race("Italian Grand Prix", 1.));
        setRaceStateMock(2020, 2);

        RaceDB.getInstance().activeRace(programStateMock).addDriver(3,lewis);
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Lewis Hamilton","Red Bull");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(6,new Driver("Marton Krooger", "Mercedes"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(5,new Driver("Marton Krooger", "Merci"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(8,new Driver("Martn Kroner", "Red Bully"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(9,new Driver("Marssaason Goger", "Red Duck"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(10,new Driver("Lewisi Hamili", "Yellow Bull"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(7,new Driver("Lais Hamiliteon", "Red Kitten"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(4,new Driver("Ewis Amilton", "Meedaios"));
        RaceDB.getInstance().activeRace(programStateMock).addDriver(1,new Driver("Arthas the Lich king", "Azeroth"));
        RaceDB.getInstance().activeYear(programStateMock).getDriver("Arthas the Lich king", "Azeroth");
        RaceDB.getInstance().activeRace(programStateMock).addDriver(2,new Driver("Arnold Swarcanegger", "Terminator"));
    }

    public void setRaceStateMock(int year, int raceNum){
        when(programStateMock.getActiveYear()).thenReturn(year);
        when(programStateMock.getActiveRaceNum()).thenReturn(raceNum);
    }

    /**
     * Checkout for the error message when previous query is override
     * @throws CommandException
     */
    @org.junit.Test
    public void validateCommand_pass() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH).thenReturn(CommandType.QUERY).thenReturn(CommandType.POINT);

        List<String> input1  = Arrays.asList("QUERY", "2020");
        List<String> input2  = Arrays.asList("QUERY", "2020", "5");
        List<String> input3  = Arrays.asList("QUERY", "2020", "2");

        Command queryCommand = new QueryCommand();

        queryCommand.validateCommand(input1, programStateMock);
        queryCommand.validateCommand(input2, programStateMock);
        queryCommand.validateCommand(input3, programStateMock);
    }

    @org.junit.Test
    public void execute_pass(){

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH).thenReturn(CommandType.QUERY).thenReturn(CommandType.POINT);

        List<String> input1  = Arrays.asList("QUERY", "2020");
        List<String> input2  = Arrays.asList("QUERY", "2020", "5");
        List<String> input3  = Arrays.asList("QUERY", "2020", "2");

        Command queryCommand = new QueryCommand();

        queryCommand.execute(input1, programStateMock);
        queryCommand.execute(input2, programStateMock);
        queryCommand.execute(input3, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_race() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RACE);

        List<String> input  = Arrays.asList("QUERY", "2020");

        Command resultCommand = new QueryCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_result() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.RESULT);

        List<String> input  = Arrays.asList("QUERY", "2020");

        Command resultCommand = new QueryCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_wrongPrevCommand_fastest() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FASTEST);

        List<String> input  = Arrays.asList("QUERY", "2020");

        Command resultCommand = new QueryCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_lessArgument() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("QUERY");

        Command resultCommand = new QueryCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_moreArgument() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input  = Arrays.asList("QUERY", "2020", "5", "0");

        Command resultCommand = new QueryCommand();

        resultCommand.validateCommand(input, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_notInteger1() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input1  = Arrays.asList("QUERY", "2020h");

        Command resultCommand = new QueryCommand();

        resultCommand.validateCommand(input1, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_notInteger2() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input1  = Arrays.asList("QUERY", "2020", "C");

        Command resultCommand = new QueryCommand();

        resultCommand.validateCommand(input1, programStateMock);
    }

    @org.junit.Test(expected = CommandException.class)
    public void validateCommand_LessRaceThanEndRaceNum() throws CommandException {

        when(programStateMock.getPrevCommand()).thenReturn(CommandType.FINISH);

        List<String> input1  = Arrays.asList("QUERY", "2020", "6");

        Command resultCommand = new QueryCommand();

        resultCommand.validateCommand(input1, programStateMock);
    }
}
