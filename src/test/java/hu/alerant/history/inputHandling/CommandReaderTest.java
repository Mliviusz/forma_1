package hu.alerant.history.inputHandling;

import static org.junit.Assert.*;

public class CommandReaderTest extends CommandReader{

    @org.junit.Test
    public void read_splitArguments_noQuotationMarks(){
        String input1 = "-QUERY 2020 -racenumber 5";
        String input2 = "-POINT PRESENT";
        String input3 = "-EXIT";

        String[] output1 = {"-QUERY", "2020", "-racenumber", "5"};
        String[] output2 = {"-POINT", "PRESENT"};
        String[] output3 = {"-EXIT"};

        assertEquals(output1, splitArguments(input1));
        assertEquals(output2, splitArguments(input2));
        assertEquals(output3, splitArguments(input3));
    }

    @org.junit.Test
    public void read_splitArguments_onlyMiddleQuotationMarks(){
        String input1 = "-RACE -year 2020 -racename \"British Grand Prix\" -racenumber 5 -pointmulti 1";
        String input2 = "-RESULT -position 1 -driver \"Lewis Hamilton\" -constructor Mercedes";

        String[] output1 = {"-RACE", "-year", "2020", "-racename", "\"British Grand Prix\"", "-racenumber", "5", "-pointmulti", "1"};
        String[] output2 = {"-RESULT", "-position", "1", "-driver", "\"Lewis Hamilton\"", "-constructor", "Mercedes"};

        assertEquals(output1, splitArguments(input1));
        assertEquals(output2, splitArguments(input2));
    }

    @org.junit.Test
    public void read_splitArguments_atTheEndQuotationMarks(){
        String input1 = "-FASTEST -driver \"Lewis Hamilton\" -constructor \"Red Bull\"";

        String[] output1 = {"-FASTEST", "-driver", "\"Lewis Hamilton\"", "-constructor", "\"Red Bull\""};

        assertEquals(output1, splitArguments(input1));
    }
}
