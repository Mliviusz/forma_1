package hu.alerant.history.inputHandling;

import hu.alerant.history.data.RaceDB;
import org.junit.Before;

import java.util.Arrays;
import java.util.List;


public class CommandExecutorImplTest {

    @Before
    public void clear() {
        RaceDB.getInstance().clear();
    }

    @org.junit.Test
    public void runCommand_pass(){
        List<String> input = Arrays.asList("RACE","2020","British Grand Prix","5","1");

        CommandExecutorImpl exec = new CommandExecutorImpl();
        exec.runCommand(input);
    }

    /**
     * Check the output for the error message
     */
    @org.junit.Test
    public void runCommand_CommandException(){
        List<String> input = Arrays.asList("RESULT","1","Lewis Hamilton","Mercedes");

        CommandExecutorImpl exec = new CommandExecutorImpl();
        exec.runCommand(input);
    }

    /**
     * Check the output for the error message
     */
    @org.junit.Test
    public void runCommand_IllegalArgument_NotExistingCommand(){
        List<String> input = Arrays.asList("RESLT","1","Lewis Hamilton","Mercedes");

        CommandExecutorImpl exec = new CommandExecutorImpl();
        exec.runCommand(input);
    }
}

