package hu.alerant.history.inputHandling;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class OptionValidatorTest {

    /**
     * This command also prints to the commandline, so check whether it really shows what we want
     */
    @org.junit.Test
    public void help(){
        OptionValidator validator = new OptionValidator();
        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);
        String[] help = {"-help"};
        String[] beforeHelp = {"valami", "-help"};
        String[] afterHelp = {"-help", "valami", "mas"};

        List<String> output = new ArrayList<>();

        when(programStateMock.getActiveYear()).thenReturn(-1);
        when(programStateMock.getQueryYear()).thenReturn(-1);
        assertEquals(output, validator.reformat(help, programStateMock));

        when(programStateMock.getActiveYear()).thenReturn(2014);
        assertEquals(output, validator.reformat(beforeHelp, programStateMock));

        when(programStateMock.getActiveYear()).thenReturn(-1);
        when(programStateMock.getQueryYear()).thenReturn(2014);
        assertEquals(output, validator.reformat(afterHelp, programStateMock));
    }

    @org.junit.Test
    public void race_valid(){
        OptionValidator validator = new OptionValidator();
        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);

        String[] input = {"-RACE", "-year", "2020", "-racename", "\"British Grand Prix\"", "-racenumber", "5", "-pointmulti", "1"};

        List<String> output = Arrays.asList("RACE","2020","British Grand Prix","5","1");

        assertEquals(output, validator.reformat(input, programStateMock));
    }

    @org.junit.Test
    public void race_invalid_lessArgumentThanNeeded(){
        OptionValidator validator = new OptionValidator();
        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);

        String[] input1 = {"-RACE", "-year", "2020", "-racename", "\"British Grand Prix\"", "-racenumber", "5", "-pointmulti"};
        String[] input2 = {"-RACE", "-year", "2020", "-racename", "-racenumber", "5", "-pointmulti", "1"};

        List<String> output = new ArrayList<>();

        assertEquals(output, validator.reformat(input1, programStateMock));
        assertEquals(output, validator.reformat(input2, programStateMock));
    }

    @org.junit.Test
    public void result_valid(){
        OptionValidator validator = new OptionValidator();
        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);

        String[] input1 = {"-RESULT", "-position", "1", "-driver", "\"Lewis Hamilton\"", "-constructor", "Mercedes"};
        String[] input2 = {"-RESULT", "-position", "7", "-driver", "\"S�bastien Bourdais\"", "-constructor", "Toro Rosso"};

        List<String> output1 = Arrays.asList("RESULT","1","Lewis Hamilton","Mercedes");
        List<String> output2 = Arrays.asList("RESULT","7","S�bastien Bourdais","Toro Rosso");

        assertEquals(output1, validator.reformat(input1, programStateMock));
        assertEquals(output2, validator.reformat(input2, programStateMock));
    }

    @org.junit.Test
    public void fastest_valid(){
        OptionValidator validator = new OptionValidator();
        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);

        String[] input1 = {"-FASTEST", "-driver", "\"Lewis Hamilton\"", "-constructor", "Mercedes"};
        String[] input2 = {"-FASTEST", "-driver", "\"S�bastien Bourdais\"", "-constructor", "Red Bull"};

        List<String> output1 = Arrays.asList("FASTEST","Lewis Hamilton","Mercedes");
        List<String> output2 = Arrays.asList("FASTEST","S�bastien Bourdais","Red Bull");

        assertEquals(output1, validator.reformat(input1, programStateMock));
        assertEquals(output2, validator.reformat(input2, programStateMock));
    }

    @org.junit.Test
    public void finish(){
        OptionValidator validator = new OptionValidator();
        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);

        String[] input1 = {"-FINISH"};

        List<String> output1 = Arrays.asList("FINISH");

        assertEquals(output1, validator.reformat(input1, programStateMock));
    }

    @org.junit.Test
    public void query_valid(){
        OptionValidator validator = new OptionValidator();
        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);

        String[] input1 = {"-QUERY", "2020"};
        String[] input2 = {"-QUERY", "2020", "-racenumber", "5"};

        List<String> output1 = Arrays.asList("QUERY","2020");
        List<String> output2 = Arrays.asList("QUERY","2020","5");

        assertEquals(output1, validator.reformat(input1, programStateMock));
        assertEquals(output2, validator.reformat(input2, programStateMock));
    }

    @org.junit.Test
    public void point_valid(){
        OptionValidator validator = new OptionValidator();
        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);

        String[] input1 = {"-POINT", "PRESENT"};

        List<String> output1 = Arrays.asList("POINT","PRESENT");

        assertEquals(output1, validator.reformat(input1, programStateMock));
    }

    @org.junit.Test
    public void exit(){
        OptionValidator validator = new OptionValidator();
        CommandExecutorImpl.ProgramState programStateMock = mock(CommandExecutorImpl.ProgramState.class);

        String[] input1 = {"-EXIT"};

        List<String> output1 = Arrays.asList("EXIT");

        assertEquals(output1, validator.reformat(input1, programStateMock));
    }
}
